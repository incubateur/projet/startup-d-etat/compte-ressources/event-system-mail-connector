import request from "supertest";
import crypto from "crypto";
import { app, server } from "../src/index"; // Ensure this path correctly references your index.ts file
import * as mailerService from "../src/services/mailer"; // Ensure the path is correct
import config from "../src/config"; // Ensure this path is correct

jest.mock("../src/services/mailer");

const generateHMACSignature = (body: string) => {
  return crypto
    .createHmac("sha256", config.webhookSecretKey)
    .update(body)
    .digest("hex");
};

describe("POST /notify", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll((done) => {
    server.close(done);
  });

  it("should send a mail and return 201", async () => {
    (mailerService.sendMail as jest.Mock).mockResolvedValueOnce({
      messageId: "12345",
    });

    const validPayload = {
      topic: "test_topic",
      event: {
        eventId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
        requestId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
        payload: {
          rneId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
          status: "validated",
          to: "montassar@lajavaness.com",
          rneName: "XpLive Découverte - Version Gratuite",
          submitDate: "2024-07-22T15:11:05.253Z",
        },
      },
    };

    const signature = generateHMACSignature(JSON.stringify(validPayload));

    const response = await request(app)
      .post("/notify")
      .set("x-webhook-signature", signature)
      .send(validPayload);

    expect(response.status).toBe(201);
    expect(response.body).toEqual({ messageId: "12345" });

    expect(mailerService.sendMail).toHaveBeenCalledWith(
      validPayload.event.payload,
    );
  });

  it("should return 400 with invalid payload", async () => {
    const invalidPayload = {
      topic: "", // Invalid, should be at least 1 character
      event: {
        eventId: "invalid_uuid", // Invalid UUID
        requestId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
        payload: {
          rneId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
          status: "invalid_status", // Invalid status value
          to: "invalid_email", // Invalid email format
          rneName: "XpLive Découverte - Version Gratuite",
          submitDate: "2024-07-22T15:11:05.253Z",
        },
      },
    };

    const signature = generateHMACSignature(JSON.stringify(invalidPayload));

    const response = await request(app)
      .post("/notify")
      .set("x-webhook-signature", signature)
      .send(invalidPayload);

    expect(response.status).toBe(400);
  });

  it("should return 401 if signature is invalid", async () => {
    const validPayload = {
      topic: "test_topic",
      event: {
        eventId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
        requestId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
        payload: {
          rneId: "df1f3419-de16-4b2b-bfb6-79a275298bf4",
          status: "validated",
          to: "montassar@lajavaness.com",
          rneName: "XpLive Découverte - Version Gratuite",
          submitDate: "2024-07-22T15:11:05.253Z",
        },
      },
    };

    const body = JSON.stringify(validPayload);
    const invalidSignature = "invalidsignature";

    const response = await request(app)
      .post("/notify")
      .set("x-webhook-signature", invalidSignature)
      .send(validPayload);

    expect(response.status).toBe(401);
    expect(response.body).toEqual({
      message: "Could not verify the authenticity of the request.",
    });
  });
});
