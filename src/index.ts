import "reflect-metadata";
import cookieParser from "cookie-parser";
import helmet from "helmet";

import express, { RequestHandler } from "express";
import swaggerUi from "swagger-ui-express";
import cors from "cors";
import passport from "passport";
import logger, { expressLogger } from "./logger";
import healthCheckController from "./middlewares/healthcheck-handler";
import routes from "./router";
import config from "./config";
import errorMiddleware from "./middlewares/error-handler";
import swagger from "./swagger";
import RouteNotFoundException from "./errors/route-not-found-exception";
import EventSystemApiRestClient from "./services/event-system-api-rest-client";

export const app = express();

export const notFoundMiddleware: RequestHandler = (_req, _res, next) => {
  next(new RouteNotFoundException());
};

process
  .on("unhandledRejection", (reason, p) => {
    console.error("Unhandled Rejection at Promise", JSON.stringify(reason), p);
  })
  .on("uncaughtException", (err) => {
    console.error("Uncaught Exception thrown", JSON.stringify(err));
    process.exit(1);
  });

/**
 * Middlewares.
 */
app.enable("trust proxy");
app.use(cors({ origin: config.cors }));
app.use(
  express.json({
    limit: "10mb",
    verify(req, res, buffer) {
      // @ts-expect-error Related to express.json middleware. We added the raw body so we can check the signature
      if (req.originalUrl.search("notify") !== -1) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        req.textBody = buffer.toString();
      }
    },
  }),
);
app.use(express.urlencoded({ limit: "10mb", extended: true }));
app.use(cookieParser());
app.use(expressLogger);
app.get("/status", healthCheckController);

if (process.env.NODE_ENV === "production") {
  app.use(helmet());
}

app.use(
  "/swagger",
  swaggerUi.serve,
  swaggerUi.setup(swagger, { explorer: true }),
);
app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swagger);
});

app.use("/", routes);
app.use(notFoundMiddleware);
app.use(errorMiddleware);

/**
 * Server starting.
 */
const PORT = config.port;

export const server = app.listen(PORT, async () => {
  logger.info(
    `Server is running in ${process.env.NODE_ENV} mode on port ${PORT}.`,
  );
  // Subscribe to topic at startup
  const eventSystemApiRestClient = new EventSystemApiRestClient();
  await eventSystemApiRestClient.subscribe();
});
