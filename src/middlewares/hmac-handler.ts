import { Request, Response, NextFunction } from "express";
import crypto from "crypto";
import config from "../config";

/**
 * Middleware to validate HMAC signature of the request.
 *
 * This middleware checks the `x-webhook-signature` header against a computed
 * HMAC SHA-256 hash of the request body using a secret key. If the signatures
 * do not match or if the signature header is missing, the request is rejected
 * with a 401 status code.
 *
 * @param {Request} req The Express request object.
 * @param {Response} res The Express response object.
 * @param {NextFunction} next The next middleware function in the stack.
 *
 * @throws {Error} Will send a 401 response if the signature is missing or invalid.
 */
const validateHMACSignature = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { headers } = req;
  // @ts-expect-error Related to express.json middleware. We added the raw body so we can check the signature. The msg is not validated if we can't read the signature and won't process the request.
  const body = req.textBody;
  const digest = crypto
    .createHmac("sha256", config.webhookSecretKey)
    .update(body)
    .digest("hex");
  if (
    !headers["x-webhook-signature"] ||
    digest !== headers["x-webhook-signature"]
  ) {
    res
      .status(401)
      .send({ message: "Could not verify the authenticity of the request." });
    return;
  }
  next();
};

export default validateHMACSignature;
