/**
 * Request handler to make every health checks possible.
 */
import type { Request, Response } from "express";
import EventSystemApiRestController from "../services/event-system-api-rest-client";

/**
 * @param req
 * @param res
 * @openapi
 * /status:
 *   get:
 *     summary: Check server status
 *     tags:
 *       - Monitoring
 *     security: []
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                 version:
 *                   type: string
 *       500:
 *         $ref: "#/responses/InternalError"
 */
const healthCheckController = async (req: Request, res: Response) => {
  // Add Kafka Check Indicator.
  // Add other checks in the array.
  const results = [
    await new EventSystemApiRestController().doHealthcheck(),
    // TODO: Add all your healthcheck here
    // await new AnotherTest().doHealthcheck(),
  ];

  // Check if there's any error, if so, sends a 500 otherwise, keeps a 200.
  const isUp = results.reduce((acc, value) => {
    return acc && value.status === "ok";
  }, true);

  res.status(isUp ? 200 : 500).send(results);
};

export default healthCheckController;
