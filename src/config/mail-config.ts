import { Property } from "ts-convict";
import type { config } from "../@types/types";

export default class Mail implements config.Mailer {
  @Property({
    doc: "Mail SMTP server host",
    default: "",
    env: "SMTP_HOST",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public host: string;

  @Property({
    doc: "Mail SMTP auth username",
    default: "",
    env: "SMTP_USERNAME",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public username: string;

  @Property({
    doc: "Mail SMTP auth password",
    default: "",
    env: "SMTP_PASSWORD",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public password: string;

  @Property({
    doc: "Mail SMTP port",
    default: "",
    env: "SMTP_PORT",
    format: Number,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public port: number;

  @Property({
    doc: "Mail sending address",
    default: "notification@compte-ressource.it.lajavaness.com",
    env: "SMTP_FROM",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public from: string;
}
