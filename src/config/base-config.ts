import { Property, Config } from "ts-convict";
import type { SchemaObj } from "convict";
import Mailer from "./mail-config";
import Jwt from "./jwt-config";
import type { config } from "../@types/types";

@Config({
  file: "./development.json",

  // optional parameter. Defaults to 'strict', can also be 'warn'
  validationMethod: "strict",
  formats: {
    notNull: {
      validate: (val: string, schema: SchemaObj) => {
        if (!val) {
          throw Error(`${schema.env} must be set`);
        }
      },
    },
  },
})
export class BaseConfig implements config.MyConfig {
  // ts-convict will use the Typescript type if no format given
  @Property({
    doc: "Express Base URL",
    default: "http://localhost",
    env: "BASE_URL",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public baseUrl: string;

  // ts-convict will use the Typescript type if no format given
  @Property({
    doc: "Express Port",
    default: "8887",
    env: "PORT",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public port: string;

  @Property(Mailer)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public mailer: config.Mailer;

  @Property(Jwt)
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public jwt: config.Jwt;

  @Property({
    doc: "Express cors origin",
    default: "http://localhost:5000",
    env: "CORS_OPTIONS",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public cors: string;

  @Property({
    doc: "Compte-Ressource resource numérique path",
    default: "compte-ressource-dev.it.lajavaness.com",
    env: "COMPTE_RESS_URL",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public compteRessourceUrl: string;

  @Property({
    doc: "Event System API URL",
    default: "https://compte-ressource-dev-es.it.lajavaness.com",
    env: "EVENT_SYSTEM_API_BASE_URL",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public eventSystemApiBaseUrl: string;

  @Property({
    doc: "Email addresses to add in cc",
    default: ["anh@lajavaness.com", "caroline.guedan@education.gouv.fr"],
    env: "EMAIL_CC",
    format: Array,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public emailCC: Array;

  @Property({
    doc: "Topics to get notified on (Comma separated list)",
    default: "order.status.update",
    env: "TOPICS_TO_CONSUME",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public topic: string;

  @Property({
    doc: "Logger level",
    default: "info",
    env: "LOG_LEVEL",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public logLevel: string;

  @Property({
    doc: "Logger JSON Enabled",
    default: false,
    format: Boolean,
    env: "LOGGER_JSON_ENABLED",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public logJsonEnabled: string;

  @Property({
    doc: "Webhook Secret Key HMAC",
    default: "ThisIsNotSoSecret",
    env: "WEBHOOK_SECRET_KEY",
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public webhookSecretKey: string;
}

export default BaseConfig;
