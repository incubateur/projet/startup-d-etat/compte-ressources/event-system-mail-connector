import { Property } from "ts-convict";
import type { config } from "../@types/types";

export default class Jwt implements config.Jwt {
  // Unmanaged property env
  public client: string = "pg";

  @Property({
    doc: "JWT Audience",
    default: "event-system",
    env: "JWT_AUDIENCE",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public audience: string;

  @Property({
    doc: "JWT Issuer",
    default: "event-system",
    env: "JWT_ISSUER",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public issuer: string;

  @Property({
    doc: "JWT Secret",
    default: "notSoSecret",
    env: "JWT_SECRET",
    format: String,
  })
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  public secret: string;
}
