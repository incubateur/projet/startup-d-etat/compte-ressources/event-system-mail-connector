import nodemailer from "nodemailer";
import mjml2html from "mjml";
import { compile } from "handlebars";
import * as fs from "node:fs";
import * as path from "node:path";
import { OrderStatusEnum } from "../@types/types";
import config from "../config";
import { EventPayloadType } from "../definitions/notifications";
import logger from "../logger";

const transporter = nodemailer.createTransport({
  host: config.mailer.host,
  port: config.mailer.port,
  secure: false,
  auth: {
    user: config.mailer.username,
    pass: config.mailer.password,
  },
  tls: {
    rejectUnauthorized: false,
  },
});

// TODO: Remove those business rule inside the message itself received.
function getTranslation(status: string) {
  switch (status) {
    case OrderStatusEnum.ORDERED:
      return "commandée";
    case OrderStatusEnum.CANCELED:
      return "annulée";
    case OrderStatusEnum.INVALIDATED:
      return "invalidée";
    case OrderStatusEnum.VALIDATED:
      return "validée";
    case OrderStatusEnum.ASSIGNED:
      return "affectée";
    case OrderStatusEnum.SUBMITTED:
      return "envoyée";
    default:
      throw new Error(`Status invalid status: ${status}`);
  }
}

function getFormatedDate(stringDate: string) {
  const date = new Date(stringDate);

  const day = String(date.getDate()).padStart(2, "0");
  const month = String(date.getMonth() + 1).padStart(2, "0");
  const year = date.getFullYear();

  return `${day}/${month}/${year}`;
}

function getTemplateFromStatus(
  rneId: string,
  topic: string,
  renName: string,
  submitDate: string,
) {
  const input = fs.readFileSync(
    path.join(__dirname, "../templates", "men.mjml"),
    { encoding: "utf8", flag: "r" },
  );
  const template = compile(input);
  const context = {
    url: `${config.compteRessourceUrl}/compte/demandes`,
    rneId,
    status: getTranslation(topic),
    renName,
    date: getFormatedDate(submitDate),
    apUrl: config.compteRessourceUrl,
  };
  return mjml2html(template(context));
}

/**
 * Send an email, with a template that fits the topic.
 * @param {EventPayloadType} event The configuration needed to send the email.
 */
export async function sendMail(event: EventPayloadType) {
  // send mail with defined transport object
  const mailOptions = {
    from: config.mailer.from, // sender address
    to: event.to, // sender address of receivers
    cc: config.emailCC,
    subject: `Votre compte-ressource a été mis à jour`, // subjectSubject line
    html: getTemplateFromStatus(
      event.rneId,
      event.status,
      event.rneName,
      event.submitDate,
    ).html, // html body
  };
  try {
    await transporter.sendMail(mailOptions);
    logger.info(`Message sent to ${event.to}`);
  } catch (error) {
    logger.error(
      `Failed to send message sent to ${event.to} from ${config.mailer.from} host ${config.mailer.host} ${config.mailer.port}`,
    );
    logger.error(error);
  }

  // Message sent: <d786aa62-4e0a-070a-47ed-0b0666549519@ethereal.email>
}
