import jwt from "jsonwebtoken";
import axios from "axios";
import axiosRetry from "axios-retry";
import config from "../config";
import { HealthcheckController, HealthcheckResult } from "../@types/types";
import logger from "../logger";
import EventSystemApiException from "../errors/event-system-api-exception";

class EventSystemApiRestController implements HealthcheckController {
  private _name: string = "event-system-api";

  generateToken() {
    const payload = { sub: "event-system-mail-connector" };
    const { secret } = config.jwt;
    console.log(this._name);
    const options = {
      audience: config.jwt.audience,
      issuer: config.jwt.issuer,
    };
    return jwt.sign(payload, secret, options);
  }

  async subscribe() {
    const data = JSON.stringify({
      topics: config.topic,
      webhook: `${config.baseUrl}/notify`,
      name: "event-system-mail-connector",
      retry: "RETRY_ALWAYS",
    });

    const options = {
      method: "post",
      maxBodyLength: Infinity,
      url: `${config.eventSystemApiBaseUrl}/api/event/subscribe`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.generateToken()}`,
      },
      data,
    };

    // Custom retry delay
    axiosRetry(axios, {
      retries: 15000,
      retryDelay: (retryCount) => {
        return retryCount * 1000;
      },
      onRetry: (retryCount, error, requestConfig) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const newVar = { code: error.code, message: error.errors };
        logger.error(newVar);
        logger.info(`Retrying ... #${retryCount}`);
      },
      onMaxRetryTimesExceeded: (error, retryCount) => {
        throw new EventSystemApiException(error);
      },
    });
    try {
      await axios.request(options);
      logger.info(`Subscribed to topic ${config.topic} successfully`);
    } catch (err) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      logger.error(err);
    }
  }

  /**
   * @returns {Promise<HealthcheckResult>}
   */
  async doHealthcheck(): Promise<HealthcheckResult> {
    const result = await fetch(`${config.eventSystemApiBaseUrl}/api/status`);
    return {
      name: this._name,
      status: result.status === 200 ? "ok" : "error",
    };
  }
}

export default EventSystemApiRestController;
