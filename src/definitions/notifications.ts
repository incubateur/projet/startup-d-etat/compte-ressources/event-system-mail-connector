import { z } from "../util/zod";
import { OrderStatusEnum } from "../@types/types";

export const eventPayloadSchema = z
  .object({
    rneId: z
      .string()
      .openapi({ example: "df1f3419-de16-4b2b-bfb6-79a275298bf4" }),
    status: z
      .nativeEnum(OrderStatusEnum)
      .openapi({ example: OrderStatusEnum.VALIDATED }),
    to: z.string().email().openapi({ example: "manuel@lajavaness.com" }),
    rneName: z
      .string()
      .openapi({ example: "XpLive Découverte - Version Gratuite" }),
    submitDate: z.string().datetime(),
  })
  .strict();

export const eventSchema = z.object({
  topic: z.string().min(1).optional(), // Zod does not enforce required by default
  event: z.object({
    eventId: z.string().uuid().optional(), // Optional fields should use `.optional()`
    requestId: z.string().uuid().optional(),
    clientId: z.string().min(1).optional(),
    principal: z.string().min(1).optional(),
    initiationType: z.string().min(1).optional(),
    sourceApplication: z.string().min(1).optional(),
    sourceEnvironment: z.string().min(1).optional(),
    sourceInstance: z.string().min(1).optional(),
    namespace: z.string().min(1).optional(),
    timestamp: z.number().int().positive().optional(),
    payload: eventPayloadSchema, // Zod uses `record()` for unknown objects
    meta: z.record(z.string()).optional(),
    additionalProp1: z.record(z.string()).optional(),
  }),
});

export type EventPayloadType = z.infer<typeof eventPayloadSchema>;
