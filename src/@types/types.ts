export declare namespace config {
  export interface KeycloakAdmin {
    secret: string; // For admin-cli client
  }

  export interface Mailer {
    host: string;
    username: string;
    password: string;
    port: number;
    from: string;
  }

  export interface Jwt {
    audience: string;
    issuer: string;
    secret: string;
  }

  export interface Database {
    client: string;
    host: string;
    database: string;
    user: string;
    password: string;
    port: number;
    pool: {
      min: number;
      max: number;
    };
  }

  export interface MyConfig {
    port: string | number;
    cors: string;
    baseUrl: string;
    mailer: Mailer;
    jwt: Jwt;
    topic: string;
    logLevel: string;
    logJsonEnabled: boolean;
    eventSystemApiBaseUrl: string;
    compteRessourceUrl: string;
    webhookSecretKey: string;
  }
}

export enum OrderStatusEnum {
  CART = "cart",
  SUBMITTED = "submitted",
  VALIDATED = "validated",
  INVALIDATED = "invalidated",
  CANCELED = "canceled",
  ORDERED = "ordered",
  ASSIGNED = "assigned",
}

export type HealthcheckResult = {
  name: string; // Name or Label of the healthcheck tested
  status: "ok" | "error";
};

export interface HealthcheckController {
  doHealthcheck: () => Promise<HealthcheckResult>;
}
