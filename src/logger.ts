import winston, { createLogger, transports, format } from "winston";
import expressWinston from "express-winston";
import config from "./config";

const logger = createLogger({
  transports: [new transports.Console()],
  format: !config.logJsonEnabled
    ? format.combine(
        format.colorize(),
        format.errors({ stack: true }),
        format.timestamp(),
        format.printf(({ timestamp, level, message, stack }) => {
          if (stack) {
            return `[${timestamp}] ${level}: ${message} - ${stack}`;
          }
          return `[${timestamp}] ${level}: ${message}`;
        }),
      )
    : format.json(),
  level: config.logLevel,
});

export const expressLogger = expressWinston.logger({
  transports: [new winston.transports.Console()],
  format: !config.logJsonEnabled
    ? winston.format.combine(
        winston.format.colorize(),
        format.timestamp(),
        format.printf(({ timestamp, level, message }) => {
          return `[${timestamp}] ${level}: ${message}`;
        }),
      )
    : format.json(),
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute(req, res) {
    return false;
  },
});

export const expressErrorLogger = expressWinston.errorLogger({
  transports: [new winston.transports.Console()],
  format: !config.logJsonEnabled
    ? winston.format.combine(winston.format.colorize())
    : winston.format.json(),
});

export default logger;
