import { TypedRequestBody } from "zod-express-middleware";
import type { Response, NextFunction } from "express";
import { eventPayloadSchema, eventSchema } from "../definitions/notifications";
import * as service from "../services/mailer";
import MailSendException from "../errors/mail-send-exception";
import logger from "../logger";

export async function sendNotification(
  req: TypedRequestBody<typeof eventSchema>,
  res: Response,
  next: NextFunction,
) {
  try {
    logger.info(
      `Received notification: ${JSON.stringify(req.body.event.payload)}`,
    );
    const result = await service.sendMail(req.body.event.payload);
    res.status(201).send(result);
  } catch (err) {
    logger.error(err);
    throw new MailSendException(req.body.event.payload.to);
  }
}
