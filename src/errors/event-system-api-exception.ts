import HttpException from "./http-exception";

class EventSystemApiException extends HttpException {
  constructor(err: unknown) {
    super(400, `Subscription to topic failed error: ${err}`);
  }
}

export default EventSystemApiException;
