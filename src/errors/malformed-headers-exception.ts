import HttpException from "./http-exception";

class MalformedHeadersException extends HttpException {
  constructor(id: string) {
    super(404, `Headers '${id}' not found`);
  }
}

export default MalformedHeadersException;
