import HttpException from "./http-exception";

class MailSendException extends HttpException {
  constructor(address: string) {
    super(
      500,
      `Mail to ${address} couldn't be sent. See server logs for details`,
    );
  }
}

export default MailSendException;
