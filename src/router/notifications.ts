import { Router } from "express";
import { validateRequestBody } from "zod-express-middleware";
import { eventSchema } from "../definitions/notifications";
import { sendNotification } from "../controllers/notifications";
import validateHMACSignature from "../middlewares/hmac-handler";

const routes = Router();

/**
 * @openapi
 * tags:
 *   - name: Notifications
 *     description: Webhook for notifications
 */

/**
 * @openapi
 * /notify:
 *  post:
 *    summary: Receive an event through this API
 *    tags:
 *      - Notifications
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    requestBody:
 *         description: Notifications
 *         required: true
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/eventSchema'
 *    responses:
 *      200:
 *        description: OK
 *      401:
 *        $ref: "#/responses/Unauthorized"
 *      500:
 *        $ref: "#/responses/InternalError"
 */
routes.post(
  "/",
  validateRequestBody(eventSchema),
  validateHMACSignature,
  sendNotification,
);

export default routes;
