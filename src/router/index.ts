import { Router } from "express";
import notifications from "./notifications";

/**
 * API Routes.
 */
const routes = Router();

routes.use("/notify", notifications);

export default routes;
