import { OpenApiGeneratorV3 } from "@asteasolutions/zod-to-openapi";
import { omit } from "lodash";
import swaggerJsdoc from "swagger-jsdoc";
import pkg from "../package.json";
import config from "./config";
import { eventSchema } from "./definitions/notifications";

const generator = new OpenApiGeneratorV3([eventSchema.openapi("eventSchema")]);
const zodComponents = generator.generateComponents();

const swaggerOptions = {
  apis: [
    `${__dirname}/index.js`,
    `${__dirname}/index.ts`,
    `${__dirname}/router/*.ts`,
    `${__dirname}/router/*.js`,
  ],
  swaggerDefinition: {
    components: {
      securitySchemes: {
        BearerAuth: {
          type: "http",
          scheme: "bearer",
          bearerFormat: "JWT",
        },
      },
      schemas: {
        ...omit(zodComponents?.components?.schemas),
        Error: {
          type: "object",
          required: ["code", "message"],
          properties: {
            code: {
              type: "number",
            },
            message: {
              type: "string",
            },
            infos: {
              nullable: true,
              type: "object",
            },
            debug: {
              nullable: true,
              type: "object",
            },
          },
        },
      },
    },
    responses: {
      InvalidPayload: {
        description: "Invalid payload error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "INVALID_PAYLOAD",
              code: 400,
              infos: {},
            },
          },
        },
      },
      InvalidCredentials: {
        description: "Invalid credentials error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "INVALID_CREDENTIALS",
              code: 401,
              infos: {},
            },
          },
        },
      },
      Unauthorized: {
        description: "Unauthorized error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "UNAUTHORIZED",
              code: 401,
            },
          },
        },
      },
      AlreadyExists: {
        description: "Already exists error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "ALREADY_EXISTS",
              code: 409,
            },
          },
        },
      },
      NotFound: {
        description: "Not found error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "NOT_FOUND",
              code: 404,
            },
          },
        },
      },
      InternalError: {
        description: "Internal error",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Error",
            },
            example: {
              message: "INTERNAL_ERROR",
              code: 500,
            },
          },
        },
      },
    },
    basePath: "/",
    host: (config.baseUrl || "").replace(/(^\w+:|^)\/\//, ""),
    info: {
      description: pkg.description,
      title: pkg.name,
      version: pkg.version,
    },
    openapi: "3.0.0",
    produces: ["application/json", "application/xml"],
    schemes: ["https", "http"],
    security: [
      {
        BearerAuth: [],
      },
    ],
  },
};

export default swaggerJsdoc(swaggerOptions);
