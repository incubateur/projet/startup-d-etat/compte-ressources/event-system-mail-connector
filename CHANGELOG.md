## [1.2.5](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.2.4...v1.2.5) (2024-08-12)


### Bug Fixes

* Fix mail sender issue ([bac66fc](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/bac66fc11cfdb4ea48dd7a1a5f3ef01d8f41adae))

## [1.2.4](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.2.3...v1.2.4) (2024-08-06)


### Bug Fixes

* Add mailing from address to env variables ([0b8b6f9](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/0b8b6f92a2f6a0f90fd813b74e22b8b0bf9ae251))

## [1.2.3](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.2.2...v1.2.3) (2024-08-05)


### Bug Fixes

* Make smtp port an env variable ([15b96b5](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/15b96b55f0ff52d3d5faf46258f4c9015c8078de))
* Make smtp port an env variable ([9bd9c7c](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/9bd9c7c62d29093d5e35e9131e2790d2e8a48d4d))

## [1.2.2](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.2.1...v1.2.2) (2024-08-05)


### Bug Fixes

* Try StartTLS option from ljn mailer ([184c51d](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/184c51d81be7769cc2eef20e79db914fb30de044))

## [1.2.1](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.2.0...v1.2.1) (2024-07-31)


### Bug Fixes

* Fix mail template ([bc21245](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/bc2124552eaf626d6b3769b3f5f972fc4a142713))

# [1.2.0](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.5...v1.2.0) (2024-07-29)


### Features

* Add cc email for template verification ([5242bc8](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/5242bc8219c174c911454a9c27421c3979702428))

## [1.1.5](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.4...v1.1.5) (2024-07-29)


### Bug Fixes

* Fix dumb typo error ([42463df](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/42463dfb2d0a525d7296c3c9fd64872d2cb2845b))

## [1.1.4](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.3...v1.1.4) (2024-07-29)


### Bug Fixes

* Fix mailer issue ([6f8e251](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/6f8e251b13b3be56ec188e1f607e082ce880ca45))

## [1.1.3](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.2...v1.1.3) (2024-07-29)


### Bug Fixes

* Fix zod payload issue ([dd8caa5](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/dd8caa51fea4b7b1c72b488c4495e0908ce4b6ea))

## [1.1.2](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.1...v1.1.2) (2024-07-29)


### Bug Fixes

* Add error logs ([fe84255](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/fe842554ff84b91cc1b87f803989daf6a072b039))

## [1.1.1](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.1.0...v1.1.1) (2024-07-26)


### Bug Fixes

* Edit topic default value ([635a2ca](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/635a2ca8a8190cb6ba1d7049c7549a81d18f6f43))

# [1.1.0](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.5...v1.1.0) (2024-07-22)


### Features

* Update mail template and add new host ([6b4be61](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/6b4be61492a93f29bf1ceb8cc0c9e6da54fbbef2))

## [1.0.5](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.4...v1.0.5) (2024-06-21)


### Bug Fixes

* Update payload because of event-system-api update ([4bfbd49](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/4bfbd49f7d1903376634063053e6fd378d4777b4))

## [1.0.4](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.3...v1.0.4) (2024-06-20)

## [1.0.3](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.2...v1.0.3) (2024-06-20)

## [1.0.2](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.1...v1.0.2) (2024-06-20)


### Bug Fixes

* Remove unneeded code in dockerfile [#2](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/issues/2) ([c0ecff3](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/c0ecff39477bb25b8f9e9f6d0cf71a0cdad4cb4c))

## [1.0.1](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/compare/v1.0.0...v1.0.1) (2024-06-20)


### Bug Fixes

* Remove unneeded code in dockerfile ([6f7212a](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/6f7212a9bc05b57fdb40c2ca15e9de0c3022a12e))

# 1.0.0 (2024-06-20)


### Bug Fixes

* Added retries inifinite to subscription ([d413fd1](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/commit/d413fd18531061633b80861ca9e3344db3f73b84))
