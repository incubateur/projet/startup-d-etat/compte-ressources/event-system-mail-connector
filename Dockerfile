FROM node:20.12.2-alpine as builder

RUN apk add --no-cache git

WORKDIR /app

COPY /src /app/src
COPY package.json /app
COPY tsconfig.json /app
COPY /.yarn /app/.yarn
COPY yarn.lock /app
COPY .yarnrc.yml /app

RUN yarn && yarn build

FROM node:20.12.2-alpine

COPY --from=builder /app /app
WORKDIR /app

CMD yarn start
