# event-system-mail-connector

[![pipeline status](https://gitlab.com/ljn/projects/education-nationale/event-system-mail-connector/badges/main/pipeline.svg)](https://gitlab.com/ljn/internal/packages/event-system-api/-/commits/main)

## Description

This is a Mail Notification Connector for the [event-system-api](https://gitlab.com/ljn/internal/packages/event-system-api/) messaging system.
It consumes messages from the event system API on a topic, and send an email on each message received.

## Devs
This project is using [NodeJS 20](https://nodejs.org/en/).

### Installation
#### NPM
`npm install`

#### Yarn
`yarn install`

### Start the service
#### Prerequisites
1. A [Event System API](https://gitlab.com/ljn/internal/packages/event-system-api/) running
2. A Mail Server or any services to send email

#### Environment variables
| Name                             | Default                                      | Description                                                                                                       |
|----------------------------------|----------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| BASE_URL                         | http://localhost                             | Base URL for Swagger documentation                                                                                |
| PORT                             | 8887                                         | Server listening port                                                                                             |
| NODE_ENV                         | development                                  | NODE Environment to use                                                                                           |
| TOPICS_TO_CONSUME                | "order.status.update"                              | List of topics Kafka to listen to (i.e "order.status.update,user.logged.in")                                            |
| EVENT_SYSTEM_API_SUBSCRIBE_URL   | compte-ressource-dev-es.it.lajavaness.com    | Event System API Base Url                                                                                         |
| COMPTE_RESS_URL                  | compte-ressource-dev-api.it.lajavaness.com   | Compte Ressource API Base Url                                                                                     |
| SMTP_HOST                        | smtp.host.com                                | SMTP Server Host                                                                                                  |
| SMTP_USERNAME                    | manuel@lajavaness.com                        | SMTP Server Authentication username                                                                               |
| SMTP_PASSWORD                    | aPassword                                    | SMTP Server Authentication password                                                                               |
| JWT_ISSUER                       | event-system                                 | Jwt issuer. Use this in conjunction of JWT_SECRET and JWT_AUDIENCE to choose a JWT authentication strategy        |
| JWT_AUDIENCE                     | event-system                                 | Jwt audience. Use this in conjunction of JWT_ISSUER and JWT_SECRET to choose a JWT authentication strategy        |
| JWT_SECRET                       | notSoSecret                                  | Jwt secret or key. Use this in conjunction of JWT_ISSUER and JWT_AUDIENCE to choose a JWT authentication strategy |
| LOG_LEVEL                        | debug                                        | General log level (['error','warn','info','debug'])                                                               |
| LOGGER_JSON_ENABLE               | false                                        | Set to **true**  to enable json logging output to the console.                                                    |

#### NPM
`npm start`

#### Yarn
`yarn start`

#### Connect to Event System API
* The topic you set here 
2. 

### Swagger
Once started, the swagger is available here.
[http://localhost:8887/swagger](http://localhost:8887/swagger)



